# Librem 5 Goodies

This project serves to create a deb package with several temporary helper bash scripts created for the Librem 5 phone. 

Bring your script here and it might be packaged :tada:

Please avoid scripts that require cloning and building packages from source.

When one of these temporary helper scripts is no longer considered necessary, it will be removed (say if features provided by the script in question are added to `phosh`).

#### Scripts added to this package:

---

|Script | Description | Dependencies | Notes |
|:--:|:--:|:--:|:--:|
|Contacts Importer| Allows to import contacts from a vcard file to GNOME Contacts. Can work with a UI and from command line interface|`yad`, `syncevolution`, `libnotify-bin`, `Evolution Data Server`| This script will be removed from this paclage when the version of GNOME-Contacts available in PureOS supports importing contacts from a vcard file. |
|Screenshot| Allows to take screenshots in the Librem 5 phone|`yad`, `grim`, `libnotify-bin`, `xdg-user-dirs`| This script will be removed from this package when there is a native app that can take screenshots in `Phosh`. |
|Screen recorder | A simple application to record the screen| `wf-recorder`, `yad`, `libnotify-bin` | This script will be removed from this package when `phosh` has better support for recording.|

**Previously removed scripts:**

- Scale the screen: A simple application to scale the screen. This is now supported in `phosh`.

#### Adding a script to this package:

- Make a new branch based on the `pureos/latest` branch and work from it.
- Make a merge request.
- Avoid scripts that download and build source code. If you need a dependency not available in PureOS, package it :D
- If your script stores files somewhere, then please use `xdg-user-dir` to set the path instead of harcoding directory names like "Documents", "Videos", this is usefull because not all users have their OS in English. See example here; https://source.puri.sm/librem5-apps/librem5-goodies/-/blob/pureos/byzantium/l5-screenshot#L27 `xdg-user-dir` is also quite usefull
- if you are not confortable in commiting to a Deb package then open an issue asking for the script to be packaged and we will work it out
